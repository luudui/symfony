<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Debug\Debug;

umask(0000);
date_default_timezone_set('UTC');

if (in_array(getenv('APPLICATION_ENV'), ['production'])) {
    /**
     * @var Composer\Autoload\ClassLoader
     */
    $loader = require __DIR__.'/../vendor/autoload.php';
    include_once __DIR__.'/../var/bootstrap.php.cache';

    $kernel = new AppKernel('prod', false);
}
else {
    /**
     * @var Composer\Autoload\ClassLoader $loader
     */
    $loader = require __DIR__.'/../vendor/autoload.php';
    Debug::enable();

    $kernel = new AppKernel('dev', true);
}

$kernel->loadClassCache();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);