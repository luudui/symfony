<?php

namespace ORN\AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="orn_user")
 * @Vich\Uploadable
 * @UniqueEntity("username")
 * @UniqueEntity("email")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fullname", type="string", length=255, nullable=true)
     */
    private $fullname;

    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="string", length=255, nullable=true)
     */
    private $avatar;

    /**
     * @Assert\Image(
     *     mimeTypes = {"image/*"}
     * )
     * @var File
     *
     * @Vich\UploadableField(mapping="media", fileNameProperty="avatar")
     */
    private $avatarFile;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_at", type="datetime")
     */

    private $createAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modify_at", type="datetime", nullable=true)
     */
    private $modifyAt;

    public function __construct()
    {
        parent::__construct();
        $this->createAt = new \DateTime();
    }

    public function setAvatarFile(File $image = null)
    {
        $this->avatarFile = $image;

        if ($image)
            $this->modifyAt = new \DateTime('now');

        return $this;
    }

    /**
     * @return File|null
     */
    public function getAvatarFile()
    {
        return $this->avatarFile;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     *
     * @return User
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set createAt
     *
     * @param \DateTime $createAt
     *
     * @return User
     */
    public function setCreateAt($createAt)
    {
        $this->createAt = $createAt;

        return $this;
    }

    /**
     * Get createAt
     *
     * @return \DateTime
     */
    public function getCreateAt()
    {
        return $this->createAt;
    }

    /**
     * Set modifyAt
     *
     * @param \DateTime $modifyAt
     *
     * @return User
     */
    public function setModifyAt($modifyAt)
    {
        $this->modifyAt = $modifyAt;

        return $this;
    }

    /**
     * Get modifyAt
     *
     * @return \DateTime
     */
    public function getModifyAt()
    {
        return $this->modifyAt;
    }
}
