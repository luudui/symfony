$(document).ready(function () {
    $('.fancybox').fancybox({
        openEffect: 'elastic',
        closeEffect: 'elastic',
        helpers: {
            title: {
                type: 'inside'
            }
        }
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('.btn-confirm').click(function () {
        if (confirm('Are you sure you want to continue?'))
            return true;
        return false;
    });
});