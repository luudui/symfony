<?php

namespace ORN\CPBundle\Controller;

use ORN\CPBundle\Form\ProfileType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ORNCPBundle:Default:index.html.twig');
    }

    public function profileAction(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('fos_user.user_manager')->updateUser($user);

            $this->get('session')->getFlashBag()->add('success', 'Your profile has been updated successfully');
            return $this->redirectToRoute('orn_cp_user_profile');
        }
        $viewData = [
            'form' => $form->createView(),
            'user' => $user
        ];
        return $this->render('ORNCPBundle:Default:profile.html.twig', $viewData);
    }
}
